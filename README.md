# k8s Cassandra

OpenShift-friendly Cassandra Image, mainly based on
https://github.com/docker-library/cassandra

Environment variables
----------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                   |    Description                    | Default                  |
| :--------------------------------- | --------------------------------- | ------------------------ |
|  `CASSANDRA_DATA_DIR`              | Cassandra Data Directory          | `/var/lib/cassandra`     |
|  `CASSANDRA_HOME`                  | Cassandra Home Directory          | `/opt/cassandra`         |
|  `CASSANDRA_HOSTNAME`              | Cassandra Hostname                | detected                 |
|  `CASSANDRA_INTERNODE_COMPRESSION` | Cassandra Inter-Nodes Compression | `none`                   |
|  `CASSANDRA_NUM_TOKENS`            | Cassandra Amount of local Tokens  | `8`                      |
|  `CASSANDRA_PARTITIONER`           | Cassandra Partitionier            | `Murmur3`                |
|  `CASSANDRA_REQUEST_SCHEDULER`     | Cassandra Request Scheduler (v3)  | `NoScheduler`            |
|  `CASSANDRA_REPLICATION_FACTOR`    | Cassandra Replication Fact. (v4)  | `1`                      |
|  `CASSANDRA_SEEDS`                 | Cassandra Seeds                   | detected                 |
|  `CASSANDRA_SEEDS_PROVIDER`        | Cassandra Seeds Provider          | `SimpleSeedProvider`     |
|  `CASSANDRA_SNITCH`                | Cassandra Snitch                  | `GossipingPropertyFile`  |
|  `CLUSTER_NAME`                    | Cassandra Cluster Name            | `test_cluster`           |
|  `POD_IP`                          | Cassandra Listen Address          | detected                 |

Cassandra Internode Compression
--------------------------------

|   Value  | Note                                 |
| :------- | ------------------------------------ |
|    `all` | compress everything                  |
|     `dc` | compress between DCs                 |
|   `none` | do not compress (better for cpu/ram) |

Cassandra Partitioners
-----------------------

|   Value             |
| :------------------ |
|  `Murmur3`          |
|  `Random`           |
|  `ByteOrdered`      |
|  `OrderPreserving`  |

See https://teddyma.gitbooks.io/learncassandra/content/replication/partitioners.html

Cassandra Snitches
-------------------

|    Name                  | Relies on                       |
| :----------------------- | ------------------------------- |
|  `Simple`                | ??                              |
|  `GossipingPropertyFile` | cassandra-rackdc.properties     |
|  `PropertyFile`          | cassandra-topology.properties   |
|  `Ec2`                   | ec2 API                         |
|  `Ec2MultiRegion`        | ec2 API + public communications |
|  `RackInferring`         | 2nd and 3rd byte of nodes IPs   |

See https://docs.datastax.com/en/archived/cassandra/3.0/cassandra/architecture/archSnitchesAbout.html

Volumes
--------

|  Volume mount point      | Description                 |
| :----------------------- | --------------------------- |
|  `/var/lib/cassandra`    | Cassandra Data Directory    |
