CASSANDRA_HOME=${CASSANDRA_HOME:-/opt/cassandra}
if test "`id -u`" -ne 0; then
    if test -s /tmp/cassandra-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to cassandra
	sed "s|^cassandra:.*|cassandra:x:`id -g`:|" /etc/group >/tmp/cassandra-group
	sed \
	    "s|^cassandra:.*|cassandra:x:`id -u`:`id -g`:cassandra:$CASSANDRA_HOME:/usr/sbin/nologin|" \
	    /etc/passwd >/tmp/cassandra-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/cassandra-passwd
    export NSS_WRAPPER_GROUP=/tmp/cassandra-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
