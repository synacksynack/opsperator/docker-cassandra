#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh
if test -z "$CASSANDRA_HOSTNAME"; then
    CASSANDRA_HOSTNAME=`hostname 2>/dev/null`
fi
CASSANDRA_DATA_DIR=${CASSANDRA_DATA_DIR:-/var/lib/cassandra}
CASSANDRA_HOME=${CASSANDRA_HOME:-/opt/cassandra}
CASSANDRA_INTERNODE_COMPRESSION=${CASSANDRA_INTERNODE_COMPRESSION:-none}
CASSANDRA_NUM_TOKENS=${CASSANDRA_NUM_TOKENS:-8}
CASSANDRA_PARTITIONER=${CASSANDRA_PARTITIONER:-Murmur3}
CASSANDRA_REPLICATION_FACTOR=${CASSANDRA_REPLICATION_FACTOR:-1}
CASSANDRA_SEEDS_PROVIDER=${CASSANDRA_SEEDS_PROVIDER:-SimpleSeedProvider}
CASSANDRA_SNITCH=${CASSANDRA_SNITCH:-GossipingPropertyFile}
CLUSTER_NAME=${CLUSTER_NAME:-test_cluster}

CASSANDRA_SERVER_ID=`echo "$CASSANDRA_HOSTNAME" | sed 's|^.*-\([0-9][0-9]*\)$|\1|'`
if ! test "$CASSANDRA_SERVER_ID" = "$CASSANDRA_HOSTNAME"; then
    CASSANDRA_BASENAME=`echo $CASSANDRA_HOSTNAME | sed "s|-$CASSANDRA_SERVER_ID$||"`
    if test "$CASSANDRA_SERVER_ID" -ne 0; then
	CASSANDRA_FULL_INIT=false
    fi
else
    CASSANDRA_BASENAME=
    CASSANDRA_SERVER_ID=0
    CASSANDRA_FULL_INIT=true
fi

if test -z "$POD_IP"; then
    POD_IP=$(cat /proc/net/fib_trie | grep "|--"  \
		| egrep -v "(0.0.0.0| 127.|.255$|.0$)" \
		| awk '{print $NF}' | head -1)
fi

list_seeds()
{
    local check append member

    check=0
    while :
    do
	append=
	member=$CASSANDRA_BASENAME-$check.$CASSANDRA_BASENAME
	if test "$CASSANDRA_BASENAME-$check" = "$CASSANDRA_HOSTNAME"; then
	    echo NOTICE: I am a seed
	    append=$POD_IP
	elif bash -c "echo dummy >/dev/tcp/$member/7000" >/dev/null 2>&1; then
	    echo NOTICE: $member is alive
	    append=$member
	elif test $check -ge 17; then
	    echo WARNING: existing for sanity - too many seeds >&2
	    echo WARNING: could be a configuration issue >&2
	    break
	fi
	if test "$append"; then
	    check=`expr $check + 1`
	    if test "$CASSANDRA_SEEDS"; then
		export CASSANDRA_SEEDS=$CASSANDRA_SEEDS,$append
	    else
		export CASSANDRA_SEEDS=$append
	    fi
	    continue
	fi
	break
    done
}

if test "$CASSANDRA_BASENAME" -a -z "$CASSANDRA_SEEDS"; then
    list_seeds
fi
if test -z "$CASSANDRA_SEEDS"; then
    if test "$CASSANDRA_SERVER_ID" = 0; then
	CASSANDRA_SEEDS=$POD_IP
    else
	echo FATAL: failed discovering neighbors
	echo FATAL: would try again later
	exit 1
    fi
fi

mkdir -p "$CASSANDRA_DATA_DIR/data" \
    "$CASSANDRA_DATA_DIR/commitlog" \
    "$CASSANDRA_DATA_DIR/saved_caches"

sed -e "s|CLUSTER_NAME_PLACEHOLDER|$CLUSTER_NAME|g" \
    -e "s|DATADIR_PLACEHOLDER|$CASSANDRA_DATA_DIR|g" \
    -e "s|ADDRESS_PLACEHOLDER|$POD_IP|g" \
    -e "s|INTERNODE_COMPRESSION_PLACEHOLDER|$CASSANDRA_INTERNODE_COMPRESSION|g" \
    -e "s|NUM_TOKENS_PLACEHOLDER|$CASSANDRA_NUM_TOKENS|g" \
    -e "s|NUM_REPLICAS_PLACEHOLDER|$CASSANDRA_REPLICATION_FACTOR|g" \
    -e "s|PARTITIONER_PLACEHOLDER|$CASSANDRA_PARTITIONER|g" \
    -e "s|SEEDS_PLACEHOLDER|$CASSANDRA_SEEDS|g" \
    -e "s|SEEDS_PROVIDER_PLACEHOLDER|$CASSANDRA_SEEDS_PROVIDER|g" \
    -e "s|SNITCH_PLACEHOLDER|$CASSANDRA_SNITCH|g" \
    /cassandra.yaml >"$CASSANDRA_CONF/cassandra.yaml"
cat /jvm-server.options >"$CASSANDRA_CONF/jvm-server.options"
cat /jvm11-server.options >"$CASSANDRA_CONF/jvm11-server.options"
cat /jvm11-server.options >"$CASSANDRA_CONF/jvm8-server.options"

exec "$CASSANDRA_HOME/bin/cassandra" -f -R
