#!/bin/sh

. /usr/local/bin/nsswrapper.sh
CASSANDRA_HOME=${CASSANDRA_HOME:-/opt/cassandra}
if test -z "$POD_IP"; then
    POD_IP=$(cat /proc/net/fib_trie | grep "|--"  \
		| egrep -v "(0.0.0.0| 127.|.255$|.0$)" \
		| awk '{print $NF}' | head -1)
fi

"$CASSANDRA_HOME/bin/nodetool" status 2>/dev/null \
    | grep "$POD_IP" >/dev/null

exit $?
