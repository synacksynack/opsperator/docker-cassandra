SKIP_SQUASH?=1
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f testcsd || echo ok
	@@docker run --name testcsd \
	    -e CASSANDRA_CLUSTER_NAME=mycluster \
	    -e CASSANDRA_DC=DC1 \
	    -e CASSANDRA_HOSTNAME=csd0 \
	    -e CASSANDRA_RACK=RACK1 \
	    -e DEBUG=yay \
	    -e HEAP_NEWSIZE=64M \
	    -e MAX_HEAP_SIZE=256M \
	    -it opsperator/cassandra

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service statefulset; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "CASSANDRA_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "CASSANDRA_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ha.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocprod
ocprod: ocbuild
	@@oc process -f deploy/openshift/run-ha.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
